<?php

Class serverStatusModule extends Provider {

  public $serverData;

  function __construct(){
    parent::__construct();
  }

  function getServerTitle($id){
    $stmt = "SELECT name FROM realmlist WHERE id = '$id'";
    $result = $this->singleQueryFetch("auth",$stmt,"name");
    $this->serverData[$id]['name'] = $result;
  }

  function getServerExpansion($id){
    $stmt = "SELECT expansion FROM realms WHERE id = '$id'";
    $result = $this->singleQueryFetch("cms",$stmt,"expansion");
    $this->serverData[$id]['expansion'] = $result;
  }

  function getServerIp($id){
    $stmt = "SELECT address FROM realmlist WHERE id = '$id'";
    $result = $this->singleQueryFetch("auth",$stmt,"address");
    $this->serverData[$id]['ip'] = $result;
  }

  function getServerPort($id){
    $stmt = "SELECT port FROM realmlist WHERE id = '$id'";
    $result = $this->singleQueryFetch("auth",$stmt,"port");
    $this->serverData[$id]['port'] = $result;
  }

  function getServerStatus($id){
    $connect = @fsockopen($this->serverData[$id]['ip'], $this->serverData[$id]['port'], $errno, $errstr, 0.2);
      if($connect){
        $this->serverData[$id]['status'] = true;
      } else{
        $this->serverData[$id]['status'] = false;
      }
    @fclose($connect);
  }

  function getServerPlayersOnline($id){
    $stmt = "SELECT online FROM characters WHERE online = '$id'";
    $result = $this->insertQuery("char",$stmt,"online");
    $this->serverData[$id]['onlinePlayers'] = mysqli_num_rows($result);
  }

  function getServerPlayersOnlineLimit($id){
    $stmt = "SELECT `limit` FROM realms WHERE id = '$id'";
    $result = $this->singleQueryFetch("cms",$stmt,"limit");
    $this->serverData[$id]['onlinePlayersLimit'] = $result;
  }

  function getServerExpansionDetails($id){
      switch($this->serverData[$id]['expansion']) {
        case "0" :
        $this->serverData[$id]['expansion_name'] = "Vanila";
        $this->serverData[$id]['expansion_icon'] = "<img src = 'assets/components/expansion_icons/vanila.png'
          alt = '".$this->serverData[$id]['expansion_name']."' title = '".$this->serverData[$id]['expansion_name']."'/>";
        break;
        case "1" :
          $this->serverData[$id]['expansion_name'] = "Burning Crusade";
          $this->serverData[$id]['expansion_icon'] = "<img src = 'assets/components/expansion_icons/bc.gif'
            alt = '".$this->serverData[$id]['expansion_name']."' title = '".$this->serverData[$id]['expansion_name']."'/>";
        Break;
        case "2" :
          $this->serverData[$id]['expansion_name'] = "Wrath Of The Lich King";
          $this->serverData[$id]['expansion_icon'] = "<img src = 'assets/components/expansion_icons/wotlk.png'
            alt = '".$this->serverData[$id]['expansion_name']."' title = '".$this->serverData[$id]['expansion_name']."'/>";
        Break;
        case "3" :
          $this->serverData[$id]['expansion_name'] = "Cataclysm";
          $this->serverData[$id]['expansion_icon'] = "<img src = 'assets/components/expansion_icons/cata.png'
            alt = '".$this->serverData[$id]['expansion_name']."' title = '".$this->serverData[$id]['expansion_name']."'/>";
        Break;
        case "4" :
          $this->serverData[$id]['expansion_name'] = "Mist Of Pandaria";
          $this->serverData[$id]['expansion_icon'] = "<img src = 'assets/components/expansion_icons/mop.png'
            alt = '".$this->serverData[$id]['expansion_name']."' title = '".$this->serverData[$id]['expansion_name']."'/>";
        Break;
        case "5" :
          $this->serverData[$id]['expansion_name'] = "Warlords Of Draenor";
          $this->serverData[$id]['expansion_icon'] = "<img src = 'assets/components/expansion_icons/warlords.png'
            alt = '".$this->serverData[$id]['expansion_name']."' title = '".$this->serverData[$id]['expansion_name']."'/>";
        Break;
        case "6" :
          $this->serverData[$id]['expansion_name'] = "Legion";
          $this->serverData[$id]['expansion_icon'] = "<img src = 'assets/components/expansion_icons/legion.png'
            alt = '".$this->serverData[$id]['expansion_name']."' title = '".$this->serverData[$id]['expansion_name']."'/>";
        Break;
    }
  }

  function prepareServerData($id){
    $this->serverData = array(
      $id = array(
      "expansion" => "","name" => "","ip" => "","port" => "",
      "status" => "","onlinePlayers" => "","onlinePlayersLimit" => "",
      "expasion_icon"=>"","expansion_name"=>""
      ),
    );
  }

  function renderRealmData($id){
      // define all stuff
      $this->prepareServerData($id);
      $this->getServerTitle($id);
      $this->getServerExpansion($id);
      $this->getServerIp($id);
      $this->getServerPort($id);
      $this->getServerStatus($id);
      $this->getServerPlayersOnline($id);
      $this->getServerPlayersOnlineLimit($id);
      $this->getServerExpansionDetails($id);
  }
}
