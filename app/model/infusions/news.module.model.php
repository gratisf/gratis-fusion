<?php

Class newsModuleModel extends Provider {

  public $newsData;
  protected $newsCount;

  function __construct(){
    parent::__construct();
    $stmt = "SELECT id FROM news WHERE id > 0";
    $this->newsCount = mysqli_num_rows($this->insertQuery("cms",$stmt));
  }

  function getNewsTitle($id){
    $stmt = "SELECT title FROM news WHERE id = '$id'";
    $result = $this->singleQueryFetch("cms",$stmt,"title");
    $this->newsData[$id]['title'] = $result;
  }

  function getNewsContent($id){
    $stmt = "SELECT content FROM news WHERE id = '$id'";
    $result = $this->singleQueryFetch("cms",$stmt,"content");
    $this->newsData[$id]['content'] = $result;
  }

  function getNewsDate($id){
    $stmt = "SELECT date FROM news WHERE id = '$id'";
    $result = $this->singleQueryFetch("cms",$stmt,"date");
    $this->newsData[$id]['date'] = $result;
  }

  function getNewsImage($id){
    $stmt = "SELECT img FROM news WHERE id = '$id'";
    $result = $this->singleQueryFetch("cms",$stmt,"img");
    $this->newsData[$id]['img'] = $result;
  }

  function prepareNewsData(){
    $this->newsdata = array(
      'id' => array(
        'title' => "",'img' => "",'content' => "",'date' => "",
      ),
    );
  }

  function renderNewsData($id){
    $this->prepareNewsData();
    $this->getNewsTitle($id);
    $this->getNewsContent($id);
    $this->getNewsDate($id);
    $this->getNewsImage($id);
  }

}
