<?php

Class navigationModuleModel extends Provider {

  protected $itemsLimit;
  public $navData;


  function __construct(){
    parent::__construct();
    $this->itemsLimit = 12;
  }

  function getNavItemTitle($item){
    $stmt = "SELECT * FROM nav WHERE id = 1";
    $result = $this->singleQueryFetch("cms",$stmt,"nav_item_".$item);
    $this->navData[$item]['title'] = $result;
  }

  function getNavItemUrl($item){
    $stmt = "SELECT * FROM nav WHERE id = 1";
    $result = $this->singleQueryFetch("cms",$stmt,"nav_item_link_".$item);
    $this->navData[$item]['url'] = $result;
  }

  function prepareNavData(){
    $i = 0;
    while($i++ < $this->itemsLimit){
      $this->navData = array(
        $i => array('title' => "", 'url' => "#"),
      );
    }

  }

  function renderNavData($item){
    $this->prepareNavData();
    $this->getNavItemTitle($item);
    $this->getNavItemUrl($item);
  }

}
