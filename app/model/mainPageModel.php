<?php

class mainPageModel extends Provider {

  // define view file
  protected $viewFile;

  // define web data
  public $webData;

  // fedine web url
  protected $url;

  // construct the provider class so we can access database
  function __construct(){
    parent::__construct();
    // now when we have access lets give values to our website data
    $stmt = "SELECT * FROM web_settings WHERE id > 0";
    $this->webData = array(
      'title'=>$this->singleQueryFetch("cms",$stmt,"title"),
      'keywords'=>$this->singleQueryFetch("cms",$stmt,"keywords"),
      'description'=>$this->singleQueryFetch("cms",$stmt,"description"),
      'template'=>$this->singleQueryFetch("cms",$stmt,"template"),
      'cache' => $this->singleQueryFetch("cms",$stmt,"cache"),
      'realmlist' => $this->singleQueryFetch("cms",$stmt,"realmlist"),
    );
  }

  // render view of the page
  function renderview($url){

    $this->url = $url;
    if(!file_exists(VIEW_PATH.$this->url.".phtml")){
      // if page is null throw the 404 error.
      include VIEW_PATH.'includes/header.phtml';
        include VIEW_PATH.'includes/404.phtml';
      include VIEW_PATH.'includes/footer.phtml';
      // <---
    } else {
      // render template / theme files
      require_once THEME_PATH.$this->webData['template']."/template.php";
      // render modules
      require_once INFUSIONS_PATH."settings.php";
      foreach($settings as $key){
        if($key['status'] == "enable") {
          if($key['model_path'] != "") {
            include INFUSION_MODEL_PATH.$key['model_path'];
          }
          include INFUSIONS_PATH.$key['path'];
        }
      }
      // <---
      // render view
      include VIEW_PATH.'includes/header.phtml';
        include VIEW_PATH.$this->url.'.phtml';
      include VIEW_PATH.'includes/footer.phtml';
      // <---
    }
  }

}
