<?php
// session
session_start();
// define directories
define('BASE_PATH',realpath(dirname( dirname( __FILE__ ))));
define('APP_PATH', BASE_PATH."/app/");
define('MODEL_PATH', APP_PATH."/model/");
define('INFUSION_MODEL_PATH', MODEL_PATH."/infusions/");
define('ASSETS_PATH', BASE_PATH."/assets/");
define('CONFIGS_PATH', BASE_PATH."/configs/");
define('INFUSIONS_PATH', BASE_PATH."/infusions/");
define('LIB_PATH', BASE_PATH."/lib/");
define('VIEW_PATH', BASE_PATH."/view/");
define('THEME_PATH', ASSETS_PATH."/templates/");
define('CACHE_PATH', BASE_PATH."/cache/");
// require database lib file
require_once LIB_PATH."mysql.lib.php";
// require base page module
require_once MODEL_PATH."mainPageModel.php";
// ob
ob_start();
