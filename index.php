<?php
require_once 'app/boot.php'; // call the boot file where we define everything we need

class page extends mainPageModel {

  protected $cacheLifeTime;

  // construct the index module
  function __construct(){
    parent::__construct();
    // set view file of index
    $this->cacheLifeTime = 5 * 60;
  }

  // define load page function and render view of the page
  function loadPage($url){
    parent::renderview($url);
  }

  function cachePage($page){
      $cachefile = CACHE_PATH.$page.".html";
      $fp = fopen($cachefile, 'w');
      fwrite($fp, ob_get_contents());
      fclose($fp);
      ob_end_flush();
  }

  function cacheExists($page){
      if(file_exists(CACHE_PATH.$page.".html")) {
        if(time() - $this->cacheLifeTime < filemtime(CACHE_PATH.$page.".html")) {
            return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
  }

  function loadCache($page){
    $view = include CACHE_PATH.$page.".html";
    return $view;
  }

}

$url = isset($_GET['p']) ? $_GET['p'] : "index";
$page = new page();

if($page->webData['cache'] == 1) {
  if($page->cacheExists($url)){
    $page->loadCache($url);
  } else {
    $page->loadPage($url);
    $page->cachePage($url);
  }

} else {
  $page->loadPage($url);
}
