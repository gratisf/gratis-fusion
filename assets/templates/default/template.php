<?php
class template extends Provider {

    function openPanel($title){
      echo '<div class="panel panel-default">';
        echo '<div class="panel-heading panel-default">'.$title.'</div>';
        echo '<div class="panel-body">';
    }

    function closePanel(){
        echo '</div>';
      echo '</div>';
    }

    function headerTop($title,$caption){
      echo '<div id="myCarousel" class="carousel slide" data-ride="carousel">';
          echo '<div class="carousel-inner" role="listbox">';
            echo '<div class="item slide_2 active">';
              echo '<div class="carousel-caption">';
                echo '<h3>'.$title.'</h3>';
                echo '<p>'.$caption.'</p>';
              echo '</div>';
            echo '</div>';
          echo '</div>';
        echo '</div>';
    }

    function slider($img1,$img2,$img3){
      echo '<div id="SlideShow" class="carousel slide" data-ride="carousel">';
      echo '<ul class="nav nav-pills nav-justified">';
        echo '<li data-target="#SlideShow" data-slide-to="0"><a href="#">Slide 1</a></li>';
        echo '<li data-target="#SlideShow" data-slide-to="1"><a href="#">Slide 2</a></li>';
        echo '<li data-target="#SlideShow" data-slide-to="2"><a href="#">Slide 3</a></li>';
      echo '</ul>';
            echo '<div class="carousel-inner" role="listbox">';
              echo '<div class="item active '.$img1.'"></div>';
              echo '<div class="item '.$img2.'"></div>';
              echo '<div class="item '.$img3.'"></div>';
            echo '</div>';
      echo '</div>';
    }


}

$tpl = new template();
