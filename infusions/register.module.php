<?php

  class register extends registerModuleModel {

    public $result;
    public $registerData;
    public $randomNumber;

    function __construct(){
      parent::__construct();
      $this->randomNumber = mt_rand(1000000,9999999);
    }

    function cleanData($data){
			$data = trim($data);
			$data = stripslashes($data);
			$data = htmlspecialchars($data);
			$string = str_replace(' ', '-', $data); // Replaces all spaces with hyphens.

			return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    function validateCaptcha($entered,$original){
      if($entered == $original){
        return true;
      } else {
        $this->result = "Captcha incorrect";
        return false;
      }
    }

    function validateInputs($data){

      $this->registerData = array(
        "username" => $this->cleanData($data["username"]),
        "password" => $this->cleanData($data["password"]),
        "password_c" => $this->cleanData($data["password_c"]),
        "email" => $data['email'],
      );

      if(strlen($this->registerData['username']) < 4){
        $this->result = "<p>Username must be longer than 4 characters.</p>";
        return false;
      }


      if(strlen($this->registerData['password']) < 6){
        $this->result = "<p>Password must be longer than 6 characters.</p>";
        return false;
      }

      if($this->registerData['password'] != $this->registerData['password_c']){
        $this->result = "<p>Passwords don't match.</p>";
        return false;
      }

      if($this->checkUsername($this->registerData['username'])){
        $this->result = "<p>Account with that name already exists.</p>";
        return false;
      }

      if($this->checkEmail($this->registerData['email'])){
        $this->result = "<p>Account with that email address already exists.</p>";
        return false;
      }

      return true;
    }


    function trigerSubmit($data){
      if($this->validateCaptcha($data['captcha_entered'],$data['captcha_original'])){
        if($this->validateInputs($data)){
          parent::getData($this->registerData); // pass data to model
          // register if everything is ok.
          $this->trigerRegister();
          $this->result = "Account <strong>".$this->registerData['username']."</strong> Created.";
        }
      }
    }

  }

  $m_register = new register();
