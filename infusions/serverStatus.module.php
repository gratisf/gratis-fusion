<?php

class serverStatus extends serverStatusModule {

  public $serverData;
  function __construct(){
    parent::__construct();
  }

  function renderRealm($id){
    $this->renderRealmData($id);
    echo '<div class = "row">';
      echo '<div class = "col-sm-6">';
      echo $this->serverData[$id]['expansion_icon'];
      echo '<span class="label label-inverse">'.$this->serverData[$id]['name'].'</span>';
      echo '</div>';
      echo '<div class = "col-sm-6">';
      if($this->serverData[$id]['status']){
        $playersPercentOnline = $this->serverData[$id]['onlinePlayers'] / $this->serverData[$id]['onlinePlayersLimit'] * 100;
        echo '<div class="progress">';
          echo '<div class="progress-bar progress-bar-danger progress-bar-striped active progress-bar-custom" role="progressbar"';
            echo 'aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: '.$playersPercentOnline.'%; min-width:1em;">';
            echo '<small>'.$this->serverData[$id]['onlinePlayers'].'</small>';
          echo '</div>';
        echo '</div>';
      } else {
        echo '<span class="label label-danger">Offline</span>';
        echo '<br />';
      }
        echo '</div>';
      echo '</div>';
  }

}

$m_serverStatus = new serverStatus();
