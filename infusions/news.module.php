<?php

class news extends newsModuleModel {

  function __construct(){
    parent::__construct();
  }

  function renderNews(){
    $i = 0;
    while($i++ < $this->newsCount){
      $this->renderNewsData($i);
      echo "</small>Posted : ".$this->newsData[$i]['date']."</small>";
      echo "<div class = 'news_img' style='background-image:url(".$this->newsData[$i]['img'].");'>";
        echo "<span class = 'news_title'>".$this->newsData[$i]['title']."</span>";
      echo "</div> <br />";
        echo "<p>".$this->newsData[$i]['content']."</p>";
      echo "<hr />";
    }
  }

}

$m_news = new news();
