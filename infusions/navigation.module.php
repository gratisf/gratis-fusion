<?php

class navigation extends navigationModuleModel {

  protected $items;

  function __construct(){
    parent::__construct();
  }

  function renderNavigation(){
      echo '<nav class = "navbar navbar-inverse no_border" role = "navigation">';
      	  echo '<div class = "container">';
      	   echo '<div class = "navbar-header">';
      	      echo '<button type = "button" class = "navbar-toggle"';
      	         echo 'data-toggle = "collapse" data-target = "#example-navbar-collapse">';
      	         echo '<span class = "sr-only">Toggle navigation</span>';
      	         echo '<span class = "icon-bar"></span>';
      	         echo '<span class = "icon-bar"></span>';
      	         echo '<span class = "icon-bar"></span>';
      	      echo '</button>';
      	   echo '</div>';
      	   echo '<div class = "collapse navbar-collapse" id = "example-navbar-collapse">';
      	      echo '<ul class = "nav navbar-nav">';
              $i = 0;
              while($i++ < $this->itemsLimit){
                $this->renderNavData($i);
                if($this->navData[$i]['url'] != ""){
                   if($this->navData[$i]['url'] == "index.php?p=login") {
      	              if(isset($_SESSION['user'])){
                        echo '<li role="presentation"><a href = "index.php?p=ucp">Account Panel</a></li>';
                      } else {
                        echo '<li role="presentation"><a href = "'.$this->navData[$i]['url'].'">'.$this->navData[$i]['title'].'</a></li>';
                      }
                   } else {
                     echo '<li role="presentation"><a href = "'.$this->navData[$i]['url'].'">'.$this->navData[$i]['title'].'</a></li>';
                   }

                } elseif($this->navData[$i]['title'] != "") {
                  echo '<li role="presentation" class = "disabled"><a href = "'.$this->navData[$i]['url'].'" >'.$this->navData[$i]['title'].'</a></li>';
                }
              }
      	      echo '</ul>';
      	   echo '</div>';
      	 echo '</diV>';
      echo '</nav>';
  }

}

$m_nav = new navigation();
