<?php
// settings file for modules
    $settings = array(
        '1'=>array( // unique incriment ID here
          'status' => 'enable', // status of module
          'path' => 'news.module.php', // path to module file
          'model_path' => 'news.module.model.php',
        ),
        '2'=>array( // unique incriment ID here
          'status' => 'enable', // status of module
          'path' => 'navigation.module.php', // path to module file
          'model_path' => 'navigation.module.model.php',
        ),
        '3'=>array( // unique incriment ID here
          'status' => 'enable', // status of module
          'path' => 'serverStatus.module.php', // path to module file
          'model_path' => 'serverStatus.module.model.php',
        ),
        '4'=>array( // unique incriment ID here
          'status' => 'enable', // status of module
          'path' => 'register.module.php', // path to module file
          'model_path' => 'register.module.model.php',
        ),
        '5'=>array( // unique incriment ID here
          'status' => 'enable', // status of module
          'path' => 'login.module.php', // path to module file
          'model_path' => 'login.module.model.php',
        ),
        '6'=>array( // unique incriment ID here
          'status' => 'enable', // status of module
          'path' => 'ucp.module.php', // path to module file
          'model_path' => 'ucp.module.model.php',
        ),
    );
