<?php

class Provider {

  // define variables
  public $conn;
  protected $conf;
  protected $list;

  function __construct(){
    // require config file and define $this->conf to array
    include CONFIGS_PATH.'db_conf.php';
    $this->conf = $confData['database'];
    $this->list = array(
      "world" => $this->conf['worldDB'],
      "char" => $this->conf['charDB'],
      "auth" => $this->conf['authDB'],
      "cms" => $this->conf['webDB'],
    );
  }

  // lets make flexible connction to database we need?
  function connect($type){

    $this->conn = mysqli_connect(
      $this->conf['host'],
      $this->conf['user'],
      $this->conf['password'],
      $this->list[$type])or die(mysqli_error($list[$type]));

    return $this->conn;
  }

  // single query fetch
  function singleQueryFetch($type,$stmt,$value){
    $query = $this->insertQuery($type,$stmt);
    $fetched = mysqli_fetch_array($query);
    return $fetched[$value];
  }

  // single query insert
  function insertQuery($connType,$stmt){
      $this->connect($connType); // ope connection
        $query = mysqli_query(
          $this->connect($connType),$stmt
        )or die(mysqli_error($this->connect($connType))); // execute query
      $this->closeConnection(); // close connection
    return $query;
  }

  // close connection to prevent memory leaks
  function closeConnection(){
      mysqli_close($this->conn);
  }

}
